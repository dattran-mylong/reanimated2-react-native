import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {ExamplesRouteItems} from '@constants';
import {ExampleRoutes} from '@types';

const Stack = createStackNavigator();

const renderNavigator = () => {
  return ExamplesRouteItems.map(item => (
    <Stack.Screen
      key={item.key}
      name={item.name}
      component={item.component}
      options={{
        title: item.title,
      }}
    />
  ));
};

export const AppNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={ExampleRoutes.home}>
      {renderNavigator()}
    </Stack.Navigator>
  );
};
