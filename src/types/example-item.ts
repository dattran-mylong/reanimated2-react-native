import {FunctionComponent} from 'react';
import {ExampleRoutes} from './route';

type Item = {
  key: string;
};

export type ExampleItem = Item & {
  title: string;
  description: string;
  route: ExampleRoutes;
};

export type ExampleRouteItem = Item & {
  title: string;
  component: FunctionComponent;
  name: ExampleRoutes;
};
