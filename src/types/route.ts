import {CompositeNavigationProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

export type StackParamList = {
  home: undefined;
  example1: undefined;
};

export type ExampleStackProps = CompositeNavigationProp<
  StackNavigationProp<StackParamList, ExampleRoutes.home>,
  StackNavigationProp<StackParamList, ExampleRoutes.example1>
>;

// eslint-disable-next-line no-shadow
export enum ExampleRoutes {
  home = 'home',
  example1 = 'example1',
}
