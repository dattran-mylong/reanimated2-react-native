import {Example1Screen, HomeScreen} from '@screens';
import {ExampleRouteItem} from '@types';
import {ExampleRoutes} from '@types';

export const ExamplesRouteItems: ExampleRouteItem[] = [
  {
    key: '1',
    title: 'Home',
    component: HomeScreen,
    name: ExampleRoutes.home,
  },
  {
    key: '2',
    title: 'Example1 Screen',
    component: Example1Screen,
    name: ExampleRoutes.example1,
  },
];
