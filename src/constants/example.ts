import {ExampleItem} from '@types';
import {ExampleRoutes} from '@types';

export const Examples: ExampleItem[] = [
  {
    key: '1',
    title: 'Home',
    description: 'This is Home',
    route: ExampleRoutes.home,
  },
  {
    key: '2',
    title: 'EXAMPLE1',
    description: 'This is EXAMPLE1 screen',
    route: ExampleRoutes.example1,
  },
];
